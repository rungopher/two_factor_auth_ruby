include Rungopher

class AccountActivationsController < ApplicationController
  before_action :logged_in_user 

  def create
    send_code current_user.phone
    flash[:success] = "Code sent."
    redirect_to account_activation_path
  end

  # Show the page at which we could enter the verify code
  def edit
    @phone = current_user.phone
  end

  def update
    params.require(:account_activation).require(:code)
    if verify? current_user.phone, params[:account_activation][:code]
      current_user.verified = true
      current_user.save
      flash[:success] = "Successfully verified."
      redirect_to current_user
    else
      flash.now[:danger] = 'Verify code was wrong'
      render 'edit'
    end
  end

  private

    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

end
