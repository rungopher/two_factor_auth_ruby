require 'excon'
require 'json'

module Rungopher

  API_KEY = 'insert-your-api-key-here'

  # sends a verify code to the user's mobile
  def send_code(phone)
    headers = { 'Content-Type': 'application/json', 'ApiKey': API_KEY }
    body = { mobile: phone }.to_json
    res = Excon.post('https://portal.rungopher.com:8087/send', :body => body, :headers => headers)
    if res.status != 200
      raise "An error occurred when sending the two factor auth code. Received status: #{res.status}, body: #{res.body}"
    end
  end

  # returns true if the code is valid for this mobile number
  def verify?(phone, code)
    headers = { 'Content-Type': 'application/json', 'ApiKey': API_KEY }
    body = { mobile: phone, verificationCode: code }.to_json
    res = Excon.post('https://portal.rungopher.com:8087/verify', :body => body, :headers => headers)
    if res.status != 200
      raise "An error occurred when verifying the two factor auth code. Received status: #{res.status}, body: #{res.body}"
    end

    result = JSON.parse(res.body)
    return result["valid"]
  end

end
