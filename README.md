# RunGopher Two Factor Auth sample application

## Using RunGopher Two Factor Auth

The main code that interacts with RunGopher Two Factor Auth resides here:

```
app/modules/rungopher.rb
```

This code uses Excon to simplify the sending of http requests to the API.

You can also check out our API documentation [*here*](https://www.rungopher.com/2fa/doc.html)


## Using the sample application

Before getting started, you'll need to fill in your own API key in this file:

```
app/modules/rungopher.rb
```

If you don't have an API key then please contact us at support@rungopher.com

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## Acknowledgements

Based on the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

Thanks for all your hard work Michael.

## License

All source code in the RunGopher Two Factor Auth sample application
is available jointly under the MIT License and the Beerware License.
