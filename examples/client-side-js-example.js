// PLEASE NOTE: This code does not make direct requests to the RunGopher server.
//              It is intended as an example of the requests you would send from your
//              client to YOUR server. 
//              Your server then makes direct requests to the RunGopher API 


function sendNewCode() {
    var data = {
        'mobile': '${mobile}'
    }

    $.ajax({
        method: "POST",
        url: "2fa/send",
        data: data
    }).done(function(json) {
        
        // The code has been sent
        
    }).fail(function(error) {
        
        // The request was not successful. Handle the error here
        
    });
}


function verifyCode() {
    var data = {
        'mobile': '${mobile}',
        'verificationCode' : $('#verificationCode').val()
    }

    $.ajax({
        method: "POST",
        url: "2fa/verify",
        data: data
    }).done(function(json) {
        if(json.success == true) {
            
            // The code is valid
            
        } else {
            
            // The code is invalid
            
        }
    }).fail(function(error) {
        
        // The request was not successful. Handle the error here
        
    });
}
